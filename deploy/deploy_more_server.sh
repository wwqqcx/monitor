#! /bin/env bash

# 以下内容是对脚本的基本信息的描述
# Name: deploy.sh
# Desc: 对项目进行打包，将内容scp拷贝至web集群组(针对静态)
# Path: shell/
# Usage:

DATE=$(date +%Y-%m%d-%H-%M-%S)
web_server="10.0.0.7 10.0.0.8"
Sdir=/opt
Ddir=/code
Name=${DATE}-${git_version}
# 1. 进入项目目录，将内容进行打包
get_code(){
        cd ${WORKSPACE} && \
        tar czf ${Sdir}/web-${Name}.tar.gz ./*
}
# 2. 将内容通过scp拷贝至web集群组
scp_web_server(){
        for host in ${web_server}
        do
                scp ${Sdir}/web-${Name}.tar.gz root@$host:${Sdir}
                ssh root@$host "mkdir -p ${Ddir}/web-${Name} && \
                                tar -xf ${Sdir}/web-${Name}.tar.gz -C ${Ddir}/web-${Name}
                                rm -rf ${Ddir}/web && \
                                ln -s ${Ddir}/web-${Name} ${Ddir}/web"
        done
}

rollback(){
	back_file=$(ssh root@10.0.0.7 "find /code -maxdepth 1 -type d -name "web-*-${git_version}"")
	for host in ${web_server}
        do
               rm -rf ${Ddir}/web && \
               ln -s ${back_file} ${Ddir}/web
        done
	
}

deploy(){
        get_code
        scp_web_server
}

if [ $deploy_env == "deploy" ];then
	if [ ${GIT_COMMIT} == ${GIT_PREVIOUS_SUCCESSFUL_COMMIT} ];then
		echo "你已经部署过该 ${git_version} 版本"
		exit 1
	else
		deploy
	fi	
elif [ $deploy_env == "rollback" ];then
	rollback
fi